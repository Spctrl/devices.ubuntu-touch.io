# Ubuntu Touch devices

The new new [Ubuntu Touch devices website](https://devices.ubuntu-touch.io)! Made using [Gridsome](https://gridsome.org/). You can start a development server by running `npm run develop`.

## Where did my device configuration go?

Device files have been moved to the [data/devices](data/devices) folder.

## Where did my Markdown content go? It's not displaying on the live webpage!

We have currently disabled Markdown rendering on each device's page given the changes in how device information is presented. Once we have most of the devices moved from the old (everything in the markdown) method to the new (most information in the YAML) method, we will re-enable rendering.

The [Adding new devices section](#adding-new-devices) will help you update your device's configuration file to the new format.

## Adding new devices

You can add new devices by adding a markdown file with a YAML metadata block under `/devices/<codename>.md`. The YAML metadata block is a snippet of YAML in your otherwise Markdown-formatted document, and is denoted by three dashes alone on the line above and below the block.

```md
---
name: My Ported Device
deviceType: phone
...
---

## You should know...

This device occasionally requests an offering of bacon. Don't ask, just provide.

## Preinstallation instructions

Before using the UBports Installer on this device, ...
```

Your YAML metadata block will have three primary sections. For more information on filling these sections, see the [YAML metadata block reference](#yaml-metadata-block-reference) below.

### Device specifications

Device specifications should be placed in the deviceInfo section of the YAML header.

### Compatibility information

Information about working or broken features should be placed in the portStatus section.

### Preinstallation instructions

The general markdown section of the file is now available for preinstallation instructions for your device. For example, if your device needs to have a certain version of Android installed prior to running the UBports Installer, those instructions should be written in Markdown after the metadata block.

Rendering of this Markdown is currently disabled. We will re-enable it after most of the devices listed on the site have migrated to the new format.

## YAML metadata block reference

The YAML metadata block should have the following format; variables in square brackets are `[optional]`:

```yml
name: <retail name of the device>
deviceType: <phone|tablet|tv|other>
maturity: <deprecated: number between 0 and 1 indicating quality of the port>
[installerAlias: <codename of the device in the installer (leave out if it is the same as file name)>]
[installLink: <link to install instructions (repo), if installer isn't available>]
[tag: <promoted|old>]
[
portStatus:
  - categoryName: <name of the category of the features>
    features:
      - id: <feature ID, you can find the complete list below>
        value: < + | - >
        bugTracker: <link to the relevant issue report if this feature is not working>
]
[
deviceInfo:
  - id: <device specification ID, you can find the complete list below>
    value: <device technical specification, model...>
]
[
contributors:
  - name: <contributor name>
    photo: <link to avatar picture>
    forum: <link to ubports forum profile>
]
[
externalLinks:
  - name: <link label>
    link: <target>
    icon: <icon for the link> # Available icons can be found in the static/img/ folder
]
```

### Feature IDs

Actors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| manualBrightness     | Manual brightness                       |
| notificationLed      | Notification LED                        |
| torchlight           | Torchlight                              |
| vibration            | Vibration                               |

Camera

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| flashlight           | Flashlight                              |
| photo                | Photo                                   |
| video                | Video                                   |
| switchCamera         | Switching between cameras               |

Cellular

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| carrierInfo          | Carrier info, signal strength           |
| dataConnection       | Data connection                         |
| calls                | Incoming, outgoing calls                |
| mms                  | MMS in, out                             |
| pinUnlock            | PIN unlock                              |
| sms                  | SMS in, out                             |
| audioRoutings        | Change audio routings                   |
| voiceCall            | Voice in calls                          |

Endurance

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| batteryLifetimeTest  | Battery lifetime > 24h from 100%        |
| noRebootTest         | No reboot needed for 1 week             |

GPU

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| uiBoot               | Boot into UI                            |
| videoAcceleration    | Hardware video playback                 |

Misc

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| anboxPatches         | Anbox patches                           |
| apparmorPatches      | AppArmor patches                        |
| batteryPercentage    | Battery percentage                      |
| offlineCharging      | Offline charging                        |
| onlineCharging       | Online charging                         |
| recoveryImage        | Recovery image                          |
| factoryReset         | Reset to factory defaults               |
| rtcTime              | RTC time                                |
| sdCard               | SD card detection and access            |
| shutdown             | Shutdown / Reboot                       |
| wirelessCharging     | Wireless charging                       |

Network

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| bluetooth            | Bluetooth                               |
| flightMode           | Flight mode                             |
| hotspot              | Hotspot                                 |
| nfc                  | NFC                                     |
| wifi                 | WiFi                                    |

Sensors

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| autoBrightness       | Automatic brightness                    |
| fingerprint          | Fingerprint reader                      |
| gps                  | GPS                                     |
| proximity            | Proximity                               |
| rotation             | Rotation                                |
| touchscreen          | Touchscreen                             |

Sound

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| earphones            | Earphones                               |
| loudspeaker          | Loudspeaker                             |
| microphone           | Microphone                              |
| volumeControl        | Volume control                          |

USB

| Feature ID           | Feature name                            |
| :------------------- | :-------------------------------------: |
| mtp                  | MTP access                              |
| adb                  | ADB access                              |
| externalMonitor      | External monitor                        |

### Device specification IDs

| Specification ID     | Specification name                      |
| :------------------- | :-------------------------------------: |
| cpu                  | CPU                                     |
| chipset              | Chipset                                 |
| gpu                  | GPU                                     |
| rom                  | Storage                                 |
| ram                  | Memory                                  |
| android              | Android Version                         |
| battery              | Battery                                 |
| display              | Display                                 |
| rearCamera           | Rear Camera                             |
| frontCamera          | Front Camera                            |
| arch                 | Architecture                            |
| dimensions           | Dimensions                              |
| weight               | Weight                                  |

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
