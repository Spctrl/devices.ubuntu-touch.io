---
name: "Google Pixel 3a"
comment: "community device"
deviceType: "phone"
maturity: .9

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/4621/google-pixel-3a-sargo'
    icon: 'yumi'
---

### Maintainer(s)

- [fredldotme](https://forums.ubports.com/user/fredldotme)

#### Kernel

- https://github.com/fredldotme/android_kernel_google_bonito

#### Device

- https://github.com/fredldotme/android_device_google_bonito

#### Halium

- https://github.com/Halium/halium-devices/blob/halium-9.0/manifests/google_sargo.xml
