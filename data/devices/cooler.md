---
name: 'Bq Aquaris M10 HD'
deviceType: 'tablet'
maturity: .94
---

Bq m10 HD devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
