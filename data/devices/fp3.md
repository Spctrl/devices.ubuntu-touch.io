---
name: "Fairphone 3"
deviceType: "phone"
maturity: .9
installerAlias: 'FP3'

deviceInfo:
  -
    id: 'cpu'
    value: 'Octa-Core Kryo 250 1.8GHz'
  -
    id: 'chipset'
    value: 'Qualcomm MSM8953 Snapdragon 632'
  -
    id: 'gpu'
    value: 'Adreno 506 650MHz'
  -
    id: 'rom'
    value: '64 GB'
  -
    id: 'ram'
    value: '4 GB'
  -
    id: 'android'
    value: '9.0 (Pie)'
  -
    id: 'battery'
    value: '3040 mAh'
  -
    id: 'display'
    value: '143 mm (5.65 in) : 2160x1080 (427 PPI) - LCD IPS Touchscreen'
  -
    id: 'rearCamera'
    value: '12 MP, LED flash'
  -
    id: 'frontCamera'
    value: '8 MP, No flash'
  -
    id: 'arch'
    value: 'arm64'
  -
    id: 'dimensions'
    value: '158 mm (6.22 in) (h), 71.8 mm (2.83 in) (w), 9.89 mm (0.39 in) (d)'
  -
    id: 'weight'
    value: '187,4 g'

contributors:
  -
    name: 'luksus42'
    photo: ''
    forum: '#'

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/5373/fairphone-3-fp3-port'
    icon: 'yumi'
  -
    name: 'Repository'
    link: 'https://gitlab.com/ubports/community-ports/android9/fairphone-3/fairphone-fp3'
    icon: 'github'
  -
    name: 'CI builds'
    link: 'https://gitlab.com/ubports/community-ports/android9/fairphone-3/fairphone-fp3/-/pipelines'
    icon: 'github'
---

### Notes

Vibration actor does not work yet.\
Anbox is currently not supported on arm64.\
Automatic brightness is not working.
