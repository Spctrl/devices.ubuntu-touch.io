---
name: 'Pinetab'
comment: 'experimental'
deviceType: 'tablet'
maturity: .1
---

The [Pinetab](https://www.pine64.org/pinetab/) is an affordable ARM-based linux tablet. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-pinetab/) is available.
