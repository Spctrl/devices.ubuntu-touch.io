---
name: "Samsung Galaxy Note 4 (910F)"
comment: "community device"
deviceType: "phone"
maturity: .8
---

The Samsung Galaxy Note 4 in the 910F variant; codename `trlte`.

### Preparatory steps

You can install Ubuntu Touch on the Samsung Galaxy Note 4 910F.

1. Ensure you have [Installed TWRP](https://twrp.me/samsung/samsunggalaxynote4qualcomm.html).
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/) in the Android settings.
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking.

### Device specifications

|               Component | Details                                                            |
| ----------------------: | ------------------------------------------------------------------ |
|                 Chipset | Qualcomm Snapdragon 805 MSM8996                                    |
|                     CPU | Quad-core 2.7 GHz Krait 450                                        |
|                     GPU | Adreno 420                                                         |
|                 Display | 5.7 inch (143.9mm) Quad HD Super AMOLED (2560 x 1440)              |
|                 Storage | 32 GB / 64 GB                                                      |
| Shipped Android Version | 4.4                                                                |
|                  Memory | 3 GB LPDDR3                                                        |
|                 Cameras | Rear: 16 Megapixel (5322×2988), Front: 3.7 Megapixel (2560 x 1440) |
|                 Battery | Removable Li-ion 3,220 mAh, 15 Watts charging speed                |
|              Dimensions | 78.6 x 153.5 x 8.5 mm                                              |
|                  Weight | 176 g                                                              |
|                 MicroSD | microSD, microSDHC, microSDXC                                      |
|            Release Date | October 2014                                                       |

### Maintainer(s)

- [tyg3rpro](https://forums.ubports.com/user/tigerpro)

#### Kernel

- https://github.com/tyg3rpro/android_kernel_samsung_trlte

#### Device

- https://github.com/tyg3rpro/android_device_samsung_trlte
- https://github.com/tyg3rpro/android_device_samsung_trltespr
- https://github.com/tyg3rpro/android_device_samsung_trltetmo
- https://github.com/tyg3rpro/android_device_samsung_trlte-common

#### Halium

- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/samsung_trlte.xml
