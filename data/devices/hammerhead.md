---
name: 'Nexus 5'
deviceType: 'phone'
portType: 'Legacy'
tag: 'promoted'
portStatus:
  - categoryName: 'Actors'
    features:
      - id: 'manualBrightness'
        value: '+'
      - id: 'notificationLed'
        value: '+'
      - id: 'torchlight'
        value: '+'
      - id: 'vibration'
        value: '+'
  - categoryName: 'Camera'
    features:
      - id: 'flashlight'
        value: '+'
      - id: 'photo'
        value: '+'
      - id: 'video'
        value: '+'
      - id: 'switchCamera'
        value: '+'
  - categoryName: 'Cellular'
    features:
      - id: 'carrierInfo'
        value: '+'
      - id: 'dataConnection'
        value: '+'
      - id: 'calls'
        value: '+'
      - id: 'mms'
        value: '+'
      - id: 'pinUnlock'
        value: '+'
      - id: 'sms'
        value: '+'
      - id: 'audioRoutings'
        value: '+'
      - id: 'voiceCall'
        value: '+'
  - categoryName: 'Endurance'
    features:
      - id: 'batteryLifetimeTest'
        value: '+'
      - id: 'noRebootTest'
        value: '+'
  - categoryName: 'GPU'
    features:
      - id: 'uiBoot'
        value: '+'
      - id: 'videoAcceleration'
        value: '-'
        bugTracker: 'https://github.com/ubports/ubuntu-touch/issues/1092'
  - categoryName: 'Misc'
    features:
      - id: 'anboxPatches'
        value: '+'
      - id: 'apparmorPatches'
        value: '+'
      - id: 'batteryPercentage'
        value: '+'
      - id: 'offlineCharging'
        value: '+'
      - id: 'onlineCharging'
        value: '+'
      - id: 'recoveryImage'
        value: '+'
      - id: 'factoryReset'
        value: '+'
      - id: 'rtcTime'
        value: '+'
      - id: 'shutdown'
        value: '+'
      - id: 'wirelessCharging'
        value: '+'
      - id: 'wirelessExternalMonitor'
        value: '-'
  - categoryName: 'Network'
    features:
      - id: 'bluetooth'
        value: '+'
      - id: 'flightMode'
        value: '+'
      - id: 'hotspot'
        value: '+'
      - id: 'nfc'
        value: '-'
        bugTracker: 'https://github.com/ubports/ubuntu-touch/issues/245'
      - id: 'wifi'
        value: '+'
  - categoryName: 'Sensors'
    features:
      - id: 'autoBrightness'
        value: '+'
      - id: 'gps'
        value: '+'
      - id: 'proximity'
        value: '+'
      - id: 'rotation'
        value: '+'
      - id: 'touchscreen'
        value: '+'
  - categoryName: 'Sound'
    features:
      - id: 'earphones'
        value: '+'
      - id: 'loudspeaker'
        value: '+'
      - id: 'microphone'
        value: '+'
      - id: 'volumeControl'
        value: '+'
  - categoryName: 'USB'
    features:
      - id: 'mtp'
        value: '+'
      - id: 'adb'
        value: '+'
      - id: 'wiredExternalMonitor'
        value: '+'
deviceInfo:
  - id: 'cpu'
    value: 'Krait 400, 2300MHz, 4 Cores'
  - id: 'chipset'
    value: 'Qualcomm Snapdragon 800, MSM8974AA'
  - id: 'gpu'
    value: 'Qualcomm Adreno 330, 450MHz, 4 Cores'
  - id: 'rom'
    value: '16/32GB'
  - id: 'ram'
    value: '2GB, 800MHz'
  - id: 'android'
    value: '4.4.3'
  - id: 'battery'
    value: '2300 mAh'
  - id: 'display'
    value: '1080x1920 pixels, 5 in'
  - id: 'rearCamera'
    value: '8MP'
  - id: 'frontCamera'
    value: '1.2MP'
externalLinks:
  - name: 'Telegram - @ubports'
    link: 'https://t.me/joinchat/ubports'
    icon: 'telegram'
  - name: 'Device Subforum'
    link: 'https://forums.ubports.com/category/56/google-nexus-5'
    icon: 'yumi'
  - name: 'Report a bug'
    link: 'https://github.com/ubports/ubuntu-touch/issues'
    icon: 'github'
seo:
  description: 'Flash your Nexus 5 smartphone with the latest version of Ubuntu Touch operating system, a privacy focus OS developed by hundreds of people.'
  keywords: 'Nexus 5, Privacy Focus, Linux Phone'
---
